package com.stackroute.activitystream.aspect;
  /* Each of the methods of DAOImpls has to be used in the given code snippet, any particular method will have all the four aspectJ annotation(@Before, 
  @After, @AfterReturning, @AfterThrowing). Note: Provided is a sample using a single method, similarly you need to write for all the methods of 
  DAOImpls. */


import java.util.Arrays;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Aspect
 public class LoggingAspect {

private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);


}